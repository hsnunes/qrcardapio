<?php

declare(strict_types=1);

namespace Hsnunes\qrpix\Entity;

class Payload {

    /**
     * ID'sdo Payload do Pix
     * @var string
     */
    const ID_PAYLOAD_FORMAT_INDICATOR = '00';
    const ID_MERCHANT_ACCOUNT_INFORMATION = '26';
    const ID_MERCHANT_ACCOUNT_INFORMATION_GUI = '00';
    const ID_MERCHANT_ACCOUNT_INFORMATION_KEY = '01';
    const ID_MERCHANT_ACCOUNT_INFORMATION_DESCRIPTION = '02';
    const ID_MERCHANT_CATEGORY_CODE = '52';
    const ID_TRANSACTION_CURRENCY = '53';
    const ID_TRANSACTION_AMOUNT = '54';
    const ID_COUNTRY_CODE = '58';
    const ID_MERCHANT_NAME = '59';
    const ID_MERCHANT_CITY = '60';
    const ID_ADDITIONAL_DATA_FIELD_TEMPLATE = '62';
    const ID_ADDITIONAL_DATA_FIELD_TEMPLATE_TXID = '05';
    const ID_CRC16 = '63';

    /**
     * Chave Pix
     * Caso seja Fone +55 91 9 num...
     * @var string $pixkey
     */
    private $pixKey;

    /**
     * Descrição do Pagamento Pix
     * @var string
     */
    private $description;

    /**
     * Titular da Conta Pix
     * @var string
     */
    private $merchantName;

    /**
     * Cidade do Titularda Conta Pix
     * @var string
     */
    private $merchantCity;

    /**
     * ID da transação Pix
     * @var string
     */
    private $txid;

    /**
     * Valor do Pix
     * @var string|null
     */
    private $amount;

    /**
     * Método responsável por definir o valor de $pixKey
     * @var [string] $pixKey
     */
    public function setPixKey($pixKey) {
        $this->pixKey = $pixKey;
        return $this;
    }

    /**
     * Método responsável por definir o valor de $description
     * @var [string] $description
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    /**
     * Método responsável por definir o valor de $merchantName
     * @var [string] $merchantName
     */
    public function setMerchantName($merchantName) {
        $this->merchantName = $merchantName;
        return $this;
    }

    /**
     * Método responsável por definir o valor de $merchantCity
     * @var [string] $merchantCity
     */
    public function setMerchantCity($merchantCity) {
        $this->merchantCity = $merchantCity;
        return $this;
    }

    /**
     * Método responsável por definir o valor de $txid
     * @var [string] $txid
     */
    public function setTxid($txid) {
        $this->txid = $txid;
        return $this;
    }

    /**
     * Método responsável por definir o valor de $amount
     * @var [float] $amount
     */
    public function setAmount($amount) {
        if ( $amount === '' ) {
            $this->amount = '';
            return $this;
        }
        $amount = (string) number_format($amount, 2, '.', '') ;
        $this->amount = $this->getValue(self::ID_TRANSACTION_AMOUNT, $amount);
        return $this;
    }

    /**
     * Método para gerar o código completo do payload do Pix
     * @return [string]
     */
    public function getPayload()
    {
        $payload = $this->getValue(self::ID_PAYLOAD_FORMAT_INDICATOR, '01').
                   $this->getMerchantAccountInformation().
                   $this->getValue(self::ID_MERCHANT_CATEGORY_CODE, '0000').
                   $this->getValue(self::ID_TRANSACTION_CURRENCY, '986').
                   $this->amount.
                   $this->getValue(self::ID_COUNTRY_CODE, 'BR').
                   $this->getValue(self::ID_MERCHANT_NAME, $this->merchantName).
                   $this->getValue(self::ID_MERCHANT_CITY, $this->merchantCity).
                   $this->getAdditionalDataFieldTemplate();
        return $payload.$this->getCRC16($payload);
        // return $payload;
    }

    /**
     * Reponsável por retornar o valor completo de um objeto do payload
     * GEtrow mais apropriado
     * @param string $id;
     * @param string $value
     * @return string $id.$size.$value
     */
    private function getValue($id, $value) {
        $size = str_pad( strlen($value), 2, '0', STR_PAD_LEFT );
        return $id.$size.$value;

    }

    /**
     * Metodo responsavel por retornar os valores completos da informação da conta
     * @return string
     */
    private function getMerchantAccountInformation()
    {
        // Domínio do banco
        $gui = $this->getValue(self::ID_MERCHANT_ACCOUNT_INFORMATION_GUI, 'BR.GOV.BCB.PIX');

        // Chave Pix
        $key = $this->getValue(self::ID_MERCHANT_ACCOUNT_INFORMATION_KEY, $this->pixKey);

        // Descrição do Pagamento
        $description = strlen($this->description) ? $this->getValue(self::ID_MERCHANT_ACCOUNT_INFORMATION_DESCRIPTION, $this->description) : '';

        // RETORNA O VALOR COMPLETO DA CONTA
        return $this->getValue(self::ID_MERCHANT_ACCOUNT_INFORMATION, $gui.$key.$description);
    }

    /**
     * Metodo responsavel por retornar os valores completos do campo adicional TXID
     * @return  [string]
     */
    private function getAdditionalDataFieldTemplate()
    {
        // GErar TXID
        $txid = $this->getValue(self::ID_ADDITIONAL_DATA_FIELD_TEMPLATE_TXID, $this->txid);

        // REtorna o valor completo
        return $this->getValue(self::ID_ADDITIONAL_DATA_FIELD_TEMPLATE, $txid);
    }

    /**
     * Metodo responsavel por calcular o valor da hash de validação do codigo pix
     * @return string
     */
    private function getCRC16($payload) {
        // ADICIONA DADOS GERAIS NO PAYLOAD
        $payload .= self::ID_CRC16.'04';

        // Dados definidos pelo Bacen
        $polinomio = 0x1021;
        $resultado = 0xFFFF;

        // Checksum
        if ( ($lenght = strlen($payload)) > 0 ) {
            for ($offset=0; $offset < $lenght; $offset++) { 
                $resultado ^= ( ord($payload[$offset]) << 8 );
                for ($bitwise=0; $bitwise < 8; $bitwise++) { 
                    if (($resultado <<= 1) & 0x10000) $resultado ^= $polinomio;
                    $resultado &= 0xFFFF;
                }
            }
        }

        // Retorna Código CRC16 de 4 Caracteres
        return self::ID_CRC16.'04'.strtoupper(dechex($resultado));
    }

}
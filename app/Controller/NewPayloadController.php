<?php

declare(strict_types=1);

namespace Hsnunes\qrpix\Controller;

use Hsnunes\qrpix\Entity\Payload;

class NewPayloadController implements Controller
{

    public function __construct()
    {}

    public function processaRequisicao(): void
    {

        $key = filter_input(INPUT_POST, 'key', FILTER_DEFAULT);
        if ($key === '' || empty($key)) {
            header('Location: /?error=chaveVazia');
            exit;
        }
        $titular  = filter_input(INPUT_POST, 'titular', FILTER_DEFAULT);
        if ($titular === '' || empty($titular)) {
            header('Location: /?error=titularVazio');
            exit;
        }
        $valor = filter_input(INPUT_POST, 'valor', FILTER_DEFAULT);
        $descricao = filter_input(INPUT_POST, 'descricao', FILTER_DEFAULT);
        $cidade = filter_input(INPUT_POST, 'cidade', FILTER_DEFAULT);
        $txtid = filter_input(INPUT_POST, 'txtid', FILTER_DEFAULT);
        
        // Instancia principal do payload
        $obPayload = (new Payload)->setPixKey($key)
                                  ->setDescription($descricao)
                                  ->setMerchantName($titular)
                                  ->setMerchantCity($cidade)
                                  ->setAmount($valor)
                                  ->setTxid($txtid);
        var_dump($obPayload);
        exit;
        
        // Codigo de pagamento do PIX
        $payloadQrCode = $obPayload->getPayload();

        require_once __DIR__ . '/../../views/show-payload.php';

    }
}
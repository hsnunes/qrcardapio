<?php

declare(strict_types=1);

namespace Hsnunes\qrpix\Controller;

class InfoContatoController implements Controller
{
    public function __construct()
    {}

    public function processaRequisicao(): void
    {
        require_once __DIR__ . '/../../views/contato-info.php';
    }
}
<?php

declare(strict_types=1);

namespace Hsnunes\qrpix\Controller;

interface Controller
{
    public function processaRequisicao(): void;
}
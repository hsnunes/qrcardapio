<?php

declare(strict_types=1);

use Hsnunes\qrpix\Controller\PayloadFormController;
use Hsnunes\qrpix\Controller\NewPayloadController;
use Hsnunes\qrpix\Controller\InfoContatoController;

return [
    'GET|/' => PayloadFormController::class,
    'POST|/payload' => NewPayloadController::class,
    'GET|/contato' => InfoContatoController::class
];
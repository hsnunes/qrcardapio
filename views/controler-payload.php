<?php

use App\Pix\Payload;
// use Mpdf\QrCode\QrCode;
// use Mpdf\QrCode\Output;

$key = filter_input(INPUT_POST, 'key', FILTER_DEFAULT);
if ($key === '' || empty($key)) {
    header('Location: /?error=chaveVazia');
    exit;
}
$titular  = filter_input(INPUT_POST, 'titular', FILTER_DEFAULT);
if ($titular === '' || empty($titular)) {
    header('Location: /?error=titularVazio');
    exit;
}
$valor = filter_input(INPUT_POST, 'valor', FILTER_DEFAULT);
$descricao = filter_input(INPUT_POST, 'descricao', FILTER_DEFAULT);
$cidade = filter_input(INPUT_POST, 'cidade', FILTER_DEFAULT);
$txtid = filter_input(INPUT_POST, 'txtid', FILTER_DEFAULT);

// Instancia principal do payload
$obPayload = (new Payload)->setPixKey($key)
                          ->setDescription($descricao)
                          ->setMerchantName($titular)
                          ->setMerchantCity($cidade)
                          ->setAmount($valor)
                          ->setTxid($txtid);

// Codigo de pagamento do PIX
$payloadQrCode = $obPayload->getPayload();

// QR CODE
// $obQrCode = new QrCode($payloadQrCode);

// Imagem do QrCode
// $image = (new Output\Png)->output($obQrCode, 400);

// header('Content-Type: image/png');
// echo $image;

?>
<!-- <img src="data:image/png;base64, <=base64_encode($image);?>"> -->

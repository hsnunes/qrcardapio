<?php require_once __DIR__ . '/headerHtml.php'; ?>

        <div class="row align-items-center g-lg-3 py-3">

            <div class="col-md-10 mx-auto">
            <form action="/payload" name="form-qrpix" method="post" class="p-2 p-md-5 border rounded-3 bg-light">
                <div class="form-floating mb-3">
                    <input name="key" value="" type="text" class="form-control" id="key" require>
                    <label for="key">Chave Pix  - Se Fone, +55919... (*)</label>
                </div>
                <div class="form-floating mb-3">
                    <input name="titular" value="" type="text" class="form-control" id="titular" require>
                    <label for="titular">Nome Titular (*) Texto Puro, sem acentuação</label>
                </div>
                <div class="form-floating mb-3">
                    <input name="valor" type="text" class="form-control" id="valor">
                    <label for="valor">Valor - Ex: 1000.00 (separado por ponto, no decimal) ou deixar vazio</label>
                </div>
                <div class="form-floating mb-3">
                    <input name="cidade" value="BELEM" type="text" class="form-control" id="cidade" readonly>
                    <label for="cidade">Cidade Texto Puro, sem acentuação</label>
                </div>
                <div class="form-floating mb-3 d-none">
                    <input name="txtid" type="text" value="hsnqrpix" class="form-control" id="txtid" readonly>
                    <label for="txtid">Um ID para o pagamento</label>
                </div>
                <button type="button" class="w-100 btn btn-lg btn-info" data-bs-toggle="modal" data-bs-target="#instrucoes">Instruções:</button>
                <button class="w-100 btn btn-lg btn-primary" type="submit">Gerar Pagamento</button>
            </form>
            </div>
            <!-- #### -->

            <div class="modal fade" id="instrucoes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Instruções QRCode - Pix</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">

                            <ol class="list-group list-group-numbered">
                                <li class="list-group-item d-flex justify-content-between align-items-start">
                                    <div class="ms-2 me-auto">
                                        <div class="fw-bold">Chave Pix</div>
                                        Sua Chave do Pix, obrigatório. Exemplo: Fone ( 91 99999 9999 ) / CPF/CNPJ ( apenas os números ).
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-start">
                                    <div class="ms-2 me-auto">
                                        <div class="fw-bold">Nome Titular</div>
                                        O nome do titular, para que seja comparado no aplicativo do banco. Sem acentos, 'ç'. Apenas letras comuns, no máximo 20 letras
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-start">
                                    <div class="ms-2 me-auto">
                                        <div class="fw-bold">Valor</div>
                                        Caso você passar um valor fixo, ou deixar o campo sem valor, para gerar qrcode com valor zerado, para o preenchimento.
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-start">
                                    <div class="ms-2 me-auto">
                                        <div class="fw-bold">Cidade</div>
                                        A cidade que sua chave Pix esteja registrada. Sem acentos, 'ç'. Apenas letras comuns
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-start">
                                    <div class="ms-2 me-auto">
                                        <div class="fw-bold">Código pessoal</div>
                                        Uma palavra reservada para identificar o pagamento, tipo: contaEnergia. Sem acentos, 'ç'. Apenas letras comuns 
                                    </div>
                                </li>
                            </ol>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

<?php require_once __DIR__ . '/footerHtml.php';
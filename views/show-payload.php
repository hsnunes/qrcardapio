<?php
    require_once __DIR__ . '/headerHtml.php';
?>

    <div class="row">
        <div class="col-md-12">
            <h1 class="mb-1">Uma maneira simples de cobrar com Pix.</h1>
            <div class="my-2 text-center">
                <img id="payload-qrimage" alt="Seu Code Payload Pix">
                <div class="mx-auto">
                    <div class="card">
                        <div class="card-body" id="payload-qrline"><?=$payloadQrCode?></div>
                        <button type="button" id="copy-payload" class="btn btn-primary d-none">Copy</button>
                    </div>
                    <div class="d-sm-flex justify-content-sm-center py-1">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="script.js"></script>
    <?php require_once __DIR__ . '/footerHtml.php';

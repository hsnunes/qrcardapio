<?php
if(file_exists(__DIR__ . '/controler-payload.php')):
    $key = "62185225200";
    $nome = "Hildeberto Nunes";
    $desc = 'Pagamento teste';
    $valor = null;
    $cidade = 'Belem';
    $txid = 'brunelo';
    require __DIR__ . '/controler-payload.php';
endif;
?>
<!doctype html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Gerador de Payload e QrCode do Pix. 08/02/2023">
    <meta name="author" content="Hilder Nunes - HsNunes">
    <title>Payload e QrCode para Pix - HsNunes</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js" integrity="sha384-mQ93GR66B00ZXjt0YO5KlohRA5SY2XofN4zfuZxLkoj1gXtW8ANNCe9d5Y3eG5eD" crossorigin="anonymous"></script>
  </head>
  <body>

    <div class="container col-xl-10 col-xxl-8 px-3 py-3">
        <div class="row align-items-center g-lg-3 py-3">
            <div class="col-lg-7 text-center text-lg-start">
                <h1 class="display-4 fw-bold lh-1 mb-3">Meus Dados PIX.</h1>
            </div>
            <div class="col-md-10 mx-auto col-lg-5">

                <div class="px-4 py-5 my-5 text-center">
                    <img src="data:image/png;base64, <?=base64_encode($image)?>" height="400" width="400">
                    <div class="col-lg-6 mx-auto">
                        <div class="card">
                            <div class="card-body">
                                <?=$payloadQrCode?>
                            </div>
                        </div>
                        <div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
                            <button type="button" class="btn btn-primary btn-sm px-4 gap-3">Copy</button>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

  </body>
</html>






<?php

declare(strict_types=1);

use Hsnunes\qrpix\Controller\Controller;
use Hsnunes\qrpix\Controller\Error404Controller;
use Hsnunes\qrpix\Controller\NewPayloadController;
use Hsnunes\qrpix\Controller\PayloadFormController;
use Hsnunes\qrpix\Controller\InfoContatoController;

require __DIR__ . '/../vendor/autoload.php';

$routes = require_once __DIR__ . '/../config/routes.php';

$pathInfo = $_SERVER['PATH_INFO'] ?? '/';
$httpMethod = $_SERVER['REQUEST_METHOD'];
$key = "$httpMethod|$pathInfo";

if ( array_key_exists($key, $routes) ) {
    $controllerClass = $routes[$key];
    $controller = new $controllerClass();
} else {
    $controller = new Error404Controller();
}

/** @var Controller $controller */
$controller->processaRequisicao();
